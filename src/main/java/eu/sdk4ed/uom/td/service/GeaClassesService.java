/**
 * 
 */
package eu.sdk4ed.uom.td.service;

import java.util.List;

import eu.sdk4ed.uom.td.controller.response.entity.ClassPackageProjectName;

/**
 * @author George Digkas <digasgeo@gmail.com>
 *
 */
public interface GeaClassesService {

	List<ClassPackageProjectName> getMoveClassRefactoringsByProjectNameAndIsNew(String projectName, boolean isNew);

}
