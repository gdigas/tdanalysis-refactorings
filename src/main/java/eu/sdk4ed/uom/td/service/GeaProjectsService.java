package eu.sdk4ed.uom.td.service;

import eu.sdk4ed.uom.td.domain.GeaProjects;

public interface GeaProjectsService {
	
	GeaProjects getMoveClassMetrics(String projectID);

}
