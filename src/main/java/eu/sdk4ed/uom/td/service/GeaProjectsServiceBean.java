package eu.sdk4ed.uom.td.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import eu.sdk4ed.uom.td.domain.GeaProjects;
import eu.sdk4ed.uom.td.persistence.GeaProjectsRepository;

@Service
public class GeaProjectsServiceBean implements GeaProjectsService {

	@Autowired
	private GeaProjectsRepository geaProjectsRepository;
	
	@Override
	public GeaProjects getMoveClassMetrics(String projectID) {

		GeaProjects moveClassMetrics = geaProjectsRepository.getMoveClassMetrics(projectID);

		return moveClassMetrics;

	}


}
