/**
 * 
 */
package eu.sdk4ed.uom.td.service;

import java.util.List;
import java.util.Set;

import eu.sdk4ed.uom.td.controller.response.entity.PrincipalMetricsIndicators;
import eu.sdk4ed.uom.td.controller.response.entity.PrincipalSummary;
import eu.sdk4ed.uom.td.controller.response.entity.Project;

/**
 * @author George Digkas <digasgeo@gmail.com>
 *
 */
public interface PrincipalMetricsService {

	Set<Project> findAllProjects();

	List<PrincipalMetricsIndicators> getPrincipalIndicatorsByProjectName(String projectID);

	PrincipalSummary getPrincipalSummaryByProjectName(String projectID);

}
