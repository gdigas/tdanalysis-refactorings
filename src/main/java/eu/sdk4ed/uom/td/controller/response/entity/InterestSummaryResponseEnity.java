/**
 * 
 */
package eu.sdk4ed.uom.td.controller.response.entity;

/**
 * @author George Digkas <digasgeo@gmail.com>
 *
 */
public class InterestSummaryResponseEnity {

	private InterestSummaryAbstract interestSummary;

	public InterestSummaryResponseEnity() { }

	public InterestSummaryResponseEnity(InterestSummaryAbstract interestSummary) {
		this.interestSummary = interestSummary;
	}

	public InterestSummaryAbstract getInterestSummary() {
		return interestSummary;
	}

	public void setInterestSummary(InterestSummaryAbstract interestSummary) {
		this.interestSummary = interestSummary;
	}

}