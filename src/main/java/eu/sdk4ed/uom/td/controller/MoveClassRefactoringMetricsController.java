package eu.sdk4ed.uom.td.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import eu.sdk4ed.uom.td.domain.GeaProjects;
import eu.sdk4ed.uom.td.service.GeaProjectsService;

@RestController
@RequestMapping(value = "/moveClassRefactoringMetrics")
public class MoveClassRefactoringMetricsController extends BaseController {

	@Autowired
	private GeaProjectsService geaProjectService;

	@CrossOrigin(origins = "*")
	@GetMapping(value = "/search", produces = MediaType.APPLICATION_JSON_VALUE)

	// copy-paste replace them
	public ResponseEntity<GeaProjects> search(@RequestParam(value = "projectID", required = true) String projectID) {
		logger.info("> search projectID: {}", projectID);

		GeaProjects moveClassMetric = geaProjectService.getMoveClassMetrics(projectID);

		logger.info("< search projectID: {}", projectID);
		return new ResponseEntity<GeaProjects>(moveClassMetric, HttpStatus.OK);
	}

}
