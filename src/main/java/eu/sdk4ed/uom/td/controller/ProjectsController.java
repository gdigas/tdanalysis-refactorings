/**
 * 
 */
package eu.sdk4ed.uom.td.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import eu.sdk4ed.uom.td.controller.response.entity.Projects;
import eu.sdk4ed.uom.td.service.PrincipalMetricsService;

/**
 * @author George Digkas <digasgeo@gmail.com>
 *
 */
@RestController
@RequestMapping(value = "/projects")
public class ProjectsController extends BaseController {
	
	@Autowired
	private PrincipalMetricsService principalMetricsService;

	@CrossOrigin(origins = "*")
	@GetMapping(value = "/projects", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Projects> getAllProjects() {
		logger.info("> get All Projects");
		
		Projects projects = new Projects(principalMetricsService.findAllProjects());

		logger.info("< get All Projects");
		return new ResponseEntity<Projects>(projects, HttpStatus.OK);
	}

}
