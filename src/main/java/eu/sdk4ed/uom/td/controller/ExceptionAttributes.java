package eu.sdk4ed.uom.td.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;

/**
 * 
 * @author George Digkas <digasgeo@gmail.com>
 * 
 */
public interface ExceptionAttributes {

	Map<String, Object> getExceptionAttributes(Exception exception, HttpServletRequest httpRequest, HttpStatus httpStatus);

}