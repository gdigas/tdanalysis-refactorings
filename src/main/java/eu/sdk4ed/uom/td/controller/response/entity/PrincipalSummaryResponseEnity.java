/**
 * 
 */
package eu.sdk4ed.uom.td.controller.response.entity;

/**
 * @author George Digkas <digasgeo@gmail.com>
 *
 */
public class PrincipalSummaryResponseEnity {

	private PrincipalSummary principalSummary;

	public PrincipalSummaryResponseEnity() { }

	public PrincipalSummaryResponseEnity(PrincipalSummary principalSummary) {
		this.principalSummary = principalSummary;
	}

	public PrincipalSummary getPrincipalSummary() {
		return principalSummary;
	}

	public void setPrincipalSummary(PrincipalSummary principalSummary) {
		this.principalSummary = principalSummary;
	}

}