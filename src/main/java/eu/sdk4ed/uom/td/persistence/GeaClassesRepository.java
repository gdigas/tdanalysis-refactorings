/**
 * 
 */
package eu.sdk4ed.uom.td.persistence;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import eu.sdk4ed.uom.td.controller.response.entity.ClassPackageProjectName;
import eu.sdk4ed.uom.td.domain.GeaClasses;

/**
 * @author George Digkas <digasgeo@gmail.com>
 *
 */
@Repository
public interface GeaClassesRepository extends JpaRepository<GeaClasses, Long> {

	@Query(value = "SELECT new eu.sdk4ed.uom.td.controller.response.entity.ClassPackageProjectName(gc.name, gp.name, gc.projectName) FROM GeaClasses gc LEFT JOIN GeaPackages gp ON gc.packageID=gp.id WHERE gc.isNew = :isNew AND gc.projectName = :projectName ORDER BY gp.name ASC")
	List<ClassPackageProjectName> getMoveClassRefactoringsByProjectNameAndIsNew(@Param("projectName") String projectName, @Param("isNew") boolean isNew);

}
