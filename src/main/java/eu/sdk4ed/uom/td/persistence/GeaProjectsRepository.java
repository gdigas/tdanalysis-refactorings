package eu.sdk4ed.uom.td.persistence;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import eu.sdk4ed.uom.td.domain.GeaProjects;

public interface GeaProjectsRepository  extends JpaRepository<GeaProjects, String> {

	@Query(value = "SELECT g FROM GeaProjects g WHERE g.name = :name")
	GeaProjects getMoveClassMetrics(@Param("name") String name);

}
